#!/usr/bin/env python

import sys
import random

def get_input(prompt=None):
	if prompt is not None:
		sys.stderr.write(prompt)
	return raw_input()

def read_map(filename):
	points = {}
	with open(filename) as file:
		for line in file:
			line = line.split()
			if len(line) < 3:
				continue
			try:
				x = int(line[0])
				y = int(line[1])
				z = int(line[2])
				if x not in points:
					points[x] = {}
				points[x][y] = z
			except ValueError:
				continue
	return points

def cache_axis_prev(values):
	"""Generate a map of axis values to their lower neighbours."""
	values = sorted(values)
	result = {}
	for i in range(len(values) - 1):
		result[values[i + 1]] = values[i]
	return result

def cache_axis_next(values):
	"""Generate a map of axis values to their higher neighbours."""
	values = sorted(values)
	result = {}
	for i in range(len(values) - 1):
		result[values[i]] = values[i + 1]
	return result

def stochastic_hill_climbing(point, x, y):
	seq    = open('seq.txt', 'w')
	x_prev = cache_axis_prev(points.keys())
	x_next = cache_axis_next(points.keys())
	sacrificial_x = points.keys()[0]
	y_prev = cache_axis_prev(points[sacrificial_x].keys())
	y_next = cache_axis_next(points[sacrificial_x].keys())
	z = points[x][y]
	print >> seq, '%d %d %d' % (x, y, z)
	while True:
		print >> sys.stderr, (
			'(current position: (%d, %d, %d))' % (x, y, z)
		)
		candidates = [] # list of (x, y, z)
		# At the time of writing this, I had been awake for 20 hours.
		# Please forgive me for the inelegance of the code below.
		if x in x_prev and y in y_prev:
			if points[x_prev[x]][y_prev[y]] > z: # SW
				candidates += [(
					       x_prev[x], y_prev[y],
					points[x_prev[x]][y_prev[y]]
				)]
		if x in x_prev:
			if points[x_prev[x]][       y ] > z: # W
				candidates += [(
					       x_prev[x],        y ,
					points[x_prev[x]][       y ]
				)]
		if x in x_prev and y in y_next:
			if points[x_prev[x]][y_next[y]] > z: # NW
				candidates += [(
					       x_prev[x], y_next[y],
					points[x_prev[x]][y_next[y]]
				)]
		if y in y_next:
			if points[       x ][y_next[y]] > z: # N
				candidates += [(
					              x , y_next[y],
					points[       x ][y_next[y]]
				)]
		if x in x_next and y in y_next:
			if points[x_next[x]][y_next[y]] > z: # NE
				candidates += [(
					       x_next[x], y_next[y],
					points[x_next[x]][y_next[y]]
				)]
		if x in x_next:
			if points[x_next[x]][       y ] > z: # E
				candidates += [(
					       x_next[x],        y ,
					points[x_next[x]][       y ]
				)]
		if x in x_next and y in y_prev:
			if points[x_next[x]][y_prev[y]] > z: # SE
				candidates += [(
					       x_next[x], y_prev[y],
					points[x_next[x]][y_prev[y]]
				)]
		if y in y_prev:
			if points[       x ][y_prev[y]] > z: # S
				candidates += [(
					              x , y_prev[y],
					points[       x ][y_prev[y]]
				)]
		if len(candidates) == 0:
			break
		sum_of_gradients = 0.0
		candidates_with_gradients = []
		for point in candidates:
			delta_x = point[0] - x
			delta_y = point[1] - y
			delta_z = point[2] - z
			rise = float(delta_z)
			run = (delta_x ** 2 + delta_y ** 2) ** 0.5
			gradient = rise / run
			sum_of_gradients += gradient
			candidates_with_gradients += [(
				point[0], point[1], point[2],
				gradient
			)]
			print >> sys.stderr, (
				'(candidate gradient: (%d, %d, %d) -> %f)' % (
					point[0], point[1], point[2],
					gradient
				)
			)
		last_cumulative_probability = 0.0
		candidates_with_cumulative_probabilities = []
		for point in candidates_with_gradients:
			cumulative_probability = (
				last_cumulative_probability +
				point[3] / sum_of_gradients
			)
			last_cumulative_probability = cumulative_probability
			candidates_with_cumulative_probabilities += [(
				point[0], point[1], point[2],
				cumulative_probability
			)]
			print >> sys.stderr, (
				(
					'(candidate cumulative probability:' +
					' (%d, %d, %d) -> %f)'
				) % (
					point[0], point[1], point[2],
					cumulative_probability
				)
			)
		dart = random.random()
		print >> sys.stderr, '(dart thrown: %f)' % dart
		for point in candidates_with_cumulative_probabilities:
			if dart <= point[3]:
				(x, y, z, discard) = point
				print >> seq, '%d %d %d' % (x, y, z)
				break
	seq.close()
	return (x, y, z)

if __name__ == '__main__':
	filename = get_input('Map data filename:              ')
	seed     = get_input('Give initial location? [yN]     ')
	seed     = seed.strip()
	if seed == 'Y' or seed == 'y':
		seed = True
	elif seed == 'N' or seed == 'n' or seed == '':
		seed = False
	else:
		seed = False
		print >> sys.stderr, "I'll take that as a no."
	points = read_map(filename)
	if seed:
		print >> sys.stderr, 'Hint: (4896, 3344) is often interesting'
		x = int(get_input('Initial value for x:            '))
		y = int(get_input('Initial value for y:            '))
	else:
		x = random.choice(points.keys())
		y = random.choice(points[x].keys())
	print stochastic_hill_climbing(points, x, y)
