#!/usr/bin/env python

import sys
import collections
import heapq

def usage():
	sys.exit('''\
Usage: <search> [<data> <heuristic> [<start> <goal>]] [-all] [-verbose]

<search>        search type
<data>          path to graph data file
<heuristic>     path to heuristic file
<start>         label of the start node (case sensitive)
<goal>          label of the goal node (case sensitive)
-all            attempt to find and display all possible paths without asking
-verbose        print more verbose output to stderr (repeat to your leisure)

The order of the command line arguments is significant.\
''')

def get_input(prompt=None):
	if prompt is not None:
		sys.stderr.write(prompt)
	return raw_input()

def read_data(filename):
	edges = []
	with open(filename) as file:
		for line in file:
			line = line.split()
			if len(line) < 3:
				continue
			try:
				edges += [(line[0], line[1], int(line[2]))]
			except ValueError:
				continue
	return edges

def read_heuristic(filename):
	goals = {}
	with open(filename) as file:
		for line in file:
			line = line.split()
			if len(line) < 2:
				continue
			try:
				goals[line[0]] = int(line[1])
			except ValueError:
				continue
	return goals

def graph_cache(edges):
	cache = {}
	for edge in edges:
		for i in range(2):
			if edge[i] not in cache:
				cache[edge[i]] = {}
			cache[edge[i]][edge[1 - i]] = edge
	return cache

class GraphSearchNode(object):
	def __init__(
		self,
		state,
		parent_node,
		action,
		path_cost,
		depth,
		heuristic
	):
		self.state = state
		self.parent_node = parent_node
		self.action = action
		self.path_cost = path_cost
		self.depth = depth
		self.heuristic = heuristic
	def __repr__(self):
		return '<GraphSearchNode %d, %d, %s>' % (
		       self.path_cost,
		       self.depth,
		       self.state
		)
	def __str__(self):
		result = '%d / %d :' % (self.path_cost, self.depth)
		for vertex in self.state.vertices:
			result += ' ' + vertex
		return result

class GraphSearchNodeG(GraphSearchNode):
	"""A GraphSearchNode which can be sorted on f(n) = g(n)."""
	def __init__(self, *args, **kwargs):
		super(GraphSearchNodeG, self).__init__(*args, **kwargs)
	def __lt__(self, other):
		return self.path_cost < other.path_cost

class GraphSearchNodeH(GraphSearchNode):
	"""A GraphSearchNode which can be sorted on f(n) = h(n)."""
	def __init__(self, *args, **kwargs):
		super(GraphSearchNodeH, self).__init__(*args, **kwargs)
	def __lt__(self, other):
		return self.heuristic < other.heuristic

class GraphSearchPath(object):
	"""An automatically simplifying, immutable sequence of vertices."""
	def __init__(self, vertices=None):
		if vertices is None:
			self.vertices = []
		else:
			self.vertices = list(vertices)
	def __repr__(self):
		return '<GraphSearchPath %s>' % repr(self.vertices)
	def __eq__(self, other):
		return self.vertices == other.vertices
	def __hash__(self):
		# very inefficient
		return 0
	def __getitem__(self, subscript):
		if isinstance(subscript, slice):
			return GraphSearchPath(self.vertices[subscript])
		else:
			return GraphSearchPath([self.vertices[subscript]])
	def __add__(self, other):
		if isinstance(other, GraphSearchPath):
			# very inefficient
			path = self
			for vertex in other.vertices:
				path = GraphSearchPath(path.vertices + vertex)
			return path
		else:
			if other in self.vertices:
				length = self.vertices.index(other) + 1
				vertices = self.vertices[0:length]
			else:
				vertices = self.vertices + [other]
			return GraphSearchPath(vertices)
	def tip(self):
		return self.vertices[-1]

class GraphSearch(object):
	def __init__(self, edges, heuristics, start, goal, debug=0):
		self.node_class = GraphSearchNode
		self.edges = list(edges)
		self.heuristics = dict(heuristics)
		self.start = start
		self.goal = goal
		self.debug = debug
		self.cache = graph_cache(self.edges)
		self.fringe = None
		self.closed = set()
		self.fringe_reset()
	def __iter__(self):
		return self
	def fringe_insert(self, node):
		raise NotImplementedError
	def fringe_empty(self):
		return len(self.fringe) == 0
	def fringe_remove(self):
		raise NotImplementedError
	def fringe_reset(self):
		raise NotImplementedError
	def order_successors(self, successors):
		"""Optionally specify the successor insertion order."""
		return successors
	def expand(self, node):
		if self.debug > 0:
			print >> sys.stderr, '(expand %s)' % node
		successors = set()
		for vertex, edge in self.cache[node.state.tip()].iteritems():
			if self.debug > 1:
				print >> sys.stderr, (
					'((node %s f=%d g=%d h=%d))' % (
						str(vertex),
						node.path_cost + edge[2] +
						self.heuristics[vertex],
						node.path_cost + edge[2],
						self.heuristics[vertex]
					)
				)
			successors.add(self.node_class(
				node.state + vertex,
				node,
				edge,
				node.path_cost + edge[2],
				node.depth + 1,
				self.heuristics[vertex]
			))
		return successors
	def next(self): # returns the next path
		self.fringe_insert(self.node_class(
			GraphSearchPath([self.start]),
			None, None, 0, 0,
			self.heuristics[self.start]
		))
		while not self.fringe_empty():
			node = self.fringe_remove()
			if node.state.tip() == self.goal:
				return node
			if node.state not in self.closed:
				self.closed.add(node.state)
				successors = self.expand(node)
				successors = self.order_successors(successors)
				for node in successors:
					self.fringe_insert(node)
		raise StopIteration()

class BestFirstSearch(GraphSearch):
	def __init__(self, *args, **kwargs):
		super(BestFirstSearch, self).__init__(*args, **kwargs)
	def fringe_insert(self, node):
		heapq.heappush(self.fringe, node)
	def fringe_remove(self):
		return heapq.heappop(self.fringe)
	def fringe_reset(self):
		self.fringe = [] # heapq

class UniformCostSearch(BestFirstSearch):
	def __init__(self, *args, **kwargs):
		super(UniformCostSearch, self).__init__(*args, **kwargs)
		self.node_class = GraphSearchNodeG

class GreedyBestFirstSearch(BestFirstSearch):
	def __init__(self, *args, **kwargs):
		super(GreedyBestFirstSearch, self).__init__(*args, **kwargs)
		self.node_class = GraphSearchNodeH

if __name__ == '__main__':
	if len(sys.argv) < 2:
		usage()
	args = collections.deque(sys.argv)
	args.popleft() # discard sys.argv[0]
	arg_search = None
	arg_data = None
	arg_heuristic = None
	arg_start = None
	arg_goal = None
	arg_all = False
	arg_verbose = 0
	while len(args) > 0:
		arg = args.popleft()
		if arg == '-all':
			arg_all = True
		elif arg == '-verbose':
			arg_verbose += 1
		elif arg_search is None:
			arg_search = arg
		elif arg_data is None:
			arg_data = arg
		elif arg_heuristic is None:
			arg_heuristic = arg
		elif arg_start is None:
			arg_start = arg
		elif arg_goal is None:
			arg_goal = arg
		else:
			usage()
	search_class = None
	if arg_search == 'branchb-search':
		search_class = UniformCostSearch
	elif arg_search == 'greedy-search':
		search_class = GreedyBestFirstSearch
	if search_class is None:
		usage()
	if arg_data is None:
		arg_data      = get_input('Graph data filename:    ')
	if arg_heuristic is None:
		arg_heuristic = get_input('Heuristic filename:     ')
	if arg_start is None:
		arg_start     = get_input('Label of starting node: ')
	if arg_goal is None:
		arg_goal      = get_input('Label of goal node:     ')
	edges = read_data(arg_data)
	heuristics = read_heuristic(arg_heuristic)
	if arg_verbose > 0:
		print >> sys.stderr, '(starting search)'
	for path in search_class(
		edges,
		heuristics,
		arg_start,
		arg_goal,
		debug=arg_verbose
	):
		print path
		if not arg_all:
			try:
				get_input('Return to continue, ^D to stop: ')
			except EOFError:
				print
				break
