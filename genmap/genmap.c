#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static bool visual;

void point(int x, int y, int z);

int main(int argc, char **argv) {
	int kind = atoi(argv[1]);
	visual = argc > 2;
	for (int j = 0; j < 7; j++) {
		for (int i = 0; i < 13; i++) {
			double x = (i - 6.0) / 6.0 * 6.0;
			double y = (3.0 - j) / 3.0 * 6.0;
			double r = sqrt(x * x + y * y);
			switch (kind) {
			case 0:
				if (i < 6)
					point(i, j, i);
				else
					point(i, j, 12 - i);
				break;
			case 1:
				if (0.0 <= r && r < 1.3)
					point(i, j, 5);
				else if (1.3 <= r && r < 2.6)
					point(i, j, 3);
				else if (2.6 <= r && r < 3.9)
					point(i, j, 2);
				else if (3.9 <= r && r < 5.2)
					point(i, j, 3);
				else if (5.2 <= r && r < 6.5)
					point(i, j, 2);
				else
					point(i, j, 0);
				break;
			}
		}
		if (visual)
			putchar('\n');
	}
	return 0;
}

void point(int x, int y, int z) {
	if (visual)
		printf("%d", z % 10);
	else
		printf("%d %d %d\n", x, y, z);
}
